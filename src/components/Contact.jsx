import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

import '../css/Contact.css';

class Contact extends Component {
    state = {  }
    render() {
        return (
            <Col className="contact-app-container" md={12}>
                <Row>
                    <Col className="content-page-title-contact" md={12}>
                            <h4>Contacto</h4>
                    </Col>
                    <Col className="contact-app-title" >
                            <h4>Personal</h4>
                            <hr/>
                            <Row style={{marginTop : '20px'}}>
                                <Col md={3} style={{marginLeft : '20px'}}><i className="far fa-envelope" style={{fontSize : '28px'}}></i></Col>
                                <Col md={6}>tiagoliver@protonmail.com</Col>
                            </Row>
                            <hr/>
                            <Row>
                                <Col md={3} style={{marginLeft : '20px', paddingLeft : '20px'}}><i className="fas fa-mobile-alt" style={{fontSize : '28px'}}></i></Col>
                                <Col md={6}>(+34) 722 274 200</Col>
                            </Row>
                            <hr/>
                            <Row>
                                <Col md={3} style={{marginLeft : '20px', paddingLeft : '20px'}}><i className="far fa-map" style={{fontSize : '28px'}}></i></Col>
                                <Col md={6}>Barcelona - España</Col>
                            </Row>

                    </Col>
                    <Col className="contact-app-title" >
                            <h4 className="contact-thanks">Gracias por su visita!</h4>
                    </Col>
                </Row>
            </Col>
        );
    }
}

export default Contact;