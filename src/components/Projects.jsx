import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

import '../css/Projects.css';

import Game1 from '../components/projects/Game1';
import Game2 from '../components/projects/Game2';

class Projects extends Component {
    state = {  }
    render() {
        return (
            <Col className="contact-app-container" md={12}>
            <Row>
                <Col className="content-page-title-projects" md={12}>
                        <h4>Proyectos</h4>
                </Col>
                <Col className="contact-app-title" >
                        <h4>Piedra, Papel, Tijeras</h4>
                        <hr/>
                        <Game1 />
                </Col>
                <Col className="contact-app-title" >
                        <h4>Número Aleatório</h4>
                        <hr/>
                        <Game2 />
                </Col>
            </Row>
            <Row style={{marginTop: '10px'}}>
                <Col className="contact-app-title" >
                        <h4>En construcción</h4>
                        <hr/>

                </Col>
            </Row>
        </Col>
        );
    }
}

export default Projects;