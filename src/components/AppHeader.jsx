import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import { NavLink} from "react-router-dom";
import {connect} from 'react-redux';
import '../css/AppHeader.css';
import '../css/NavBar.css';

/**
 IMG IMPORTS
 */

 import Avatar from '../img/profile4x4.jpg'

class AppHeader extends Component {


    render() {

        let slideUpAnimation ='';
        if(this.props.state.open===true){
            slideUpAnimation = 'slide-animation-up'
        }
        if(this.props.state.open===false && this.props.state.closed !== false){
            slideUpAnimation = 'slide-animation-up-closed'
        }

        return (
            <Col className={slideUpAnimation + " header-container"} md={12}>
            <Col  md={12} className="header-profile-image">
               <div className="app-page-name">
               <img src={Avatar} alt="Profile"/>
               <h4>Tiago Oliver</h4>
               </div>
            </Col>
            <Col className="header-menu-container">
                <Col className="navbar-menu" md={12}>
                    <ul className="navbar-links">
                    <li className="app-menu-links" style={{width : '60px'}}> <NavLink to="/about" onClick={()=>this.props.slideUp()}>inicio</NavLink> </li>
                    <li className="app-menu-links"> <NavLink to="/resume" onClick={()=>this.props.slideUp()}>curriculum</NavLink> </li>
                    <li className="app-menu-links"> <NavLink to="/projects" onClick={()=>this.props.slideUp()}>proyectos</NavLink> </li>
                    <li className="app-menu-links"> <NavLink to="/contact" onClick={()=>this.props.slideUp()}>contacto</NavLink> </li>
                    </ul>
                </Col>
            </Col>
            </Col>
        );
    }
}

export default connect()(AppHeader);