import React, { Component } from 'react';

class NotFound extends Component {
    render() {
        return (
            <h1>Oops, seems that I can't find the page you're looking for :(</h1>
        );
    }
}

export default NotFound;