import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

import '../css/Home.css';

class Home extends Component {
    state = {}
    render() {
        return (
            <Col className="contact-app-container" md={12}>
            <Row>
                <Col className="about-app-title" >
                        <h1>¡Hola!, me llamo <span style={{color: '#222920'}}>Tiago Oliver</span></h1>
                        <h4>¡Encantado en conocerte!</h4>
                        <br />
                        <p style={{textAlign: 'justify'}}>Amante de la tecnología y con una gran capacidad de resolución de problemas, soy una persona alegre, creativa, paciente y de rápido aprendizaje, me apasiona los retos, el trabajo en equipo, un entorno multicultural y aportar ideas a mi grupo de trabajo.
                        </p>
                </Col>
                <Col className="about-app-title" >
                        <p style={{textAlign: 'justify'}}>Esta página ha sido desarollada desde cero, utilizando las siguientes tecnologías</p>
                        <h4>React JS</h4>
                        <h4>CSS3</h4>
                        <h4>HTML5</h4>
                        <h4>Javascript</h4>
                        <p style={{marginTop : '30px'}}>y mi <span style={{color : '#222920'}}>mente creativa</span> :)</p>
                </Col>
            </Row>
        </Col>
        );
    }
}

export default Home;