import React, { Component } from 'react';
import { Col, Row, Modal, Button } from 'react-bootstrap';

import './css/Game1.css';

class Game1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            computerPlay: '',
            userPlay: '',
            gameAnimation : '',
            show: false,
        }
        this.gamePlay = this.gamePlay.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }


    gamePlay(value) {
        let randomNumber = Math.floor(Math.random() * 6);
        let gameOptions = ["rock", "paper", "scissors", "paper", "scissors","rock"];
        let computerPlay = gameOptions[randomNumber];
        this.setState({
            userPlay: value,
            computerPlay : computerPlay, 
            gameAnimation : 'game-animation',
        })
    }
    render() {
        //Game Logic

        let gameResult = '';

        if(this.state.userPlay === 'rock'){
            if(this.state.computerPlay === 'scissors'){
                gameResult = 'Rock beats scissors, you win';
            }
            if(this.state.computerPlay === 'paper'){
                gameResult = 'Paper beats rock, computer wins';
            }
            if(this.state.computerPlay === 'rock'){
                gameResult = 'Rock and Rock, nobody wins';
            }
        }
        if(this.state.userPlay === 'paper'){
            if(this.state.computerPlay === 'scissors'){
                gameResult = 'Scissors beats paper, computer wins';
            }
            if(this.state.computerPlay === 'paper'){
                gameResult = 'Paper and Paper, nobody wins';
            }
            if(this.state.computerPlay === 'rock'){
                gameResult = 'Paper beats rock, you win';
            }
        }
        if(this.state.userPlay === 'scissors'){
            if(this.state.computerPlay === 'scissors'){
                gameResult = 'Scissors and scrissors, nobody wins';
            }
            if(this.state.computerPlay === 'paper'){
                gameResult = 'Scissors beats papper, you win!';
            }
            if(this.state.computerPlay === 'rock'){
                gameResult = 'Rock beats scissors, computer wins';
            }
        }
        return (
            <>
                <Col>
                <p style={{textAlign : 'justify'}}>
                    Utilizando los <code>states</code> simple de React y un poco de mágia JavaScript, podemos hacer un juego sencillo de "Piedra, Papel, Tijeras".
                </p>
                </Col>
                <br/>
                <Col md={12} style={{textAlign: 'center'}}>
                <Button variant="secondary" onClick={this.handleShow}>
                    Launch Game
                </Button>
                </Col>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header className="game-modal-title">
                        <Modal.Title style={{margin : 'auto'}}>Rock, Paper, Scissors</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Col className="game1_container">
                            <Col md={12} className={"game1_display "+this.state.gameAnimation}>
                                <Row md={12}>
                                    <Col md={6} className="game1_user_play">
                                    <h4>You</h4>
                                    <i className={"far fa-hand-"+ this.state.userPlay}></i>
                                    <p>{this.state.userPlay}</p>
                                    </Col>
                                    <Col md={6} className="game1_computer_play">
                                    <h4>Computer</h4>
                                    <i className={"far fa-hand-"+ this.state.computerPlay}></i>
                                    <p>{this.state.computerPlay}</p>
                                    </Col>
                                </Row>
                                <Col className="game_result">{gameResult}</Col>
                            </Col>
                            <Col className="game1_options">
                                <Row>
                                    <Col md={3} onClick={()=>this.gamePlay('rock')}>
                                    <i className="far fa-hand-rock"></i>
                                    <p>rock</p>
                                    </Col>
                                    <Col md={3} onClick={()=>this.gamePlay('paper')}>
                                    <i className="far fa-hand-paper"></i>
                                    <p>paper</p>
                                    </Col>
                                    <Col md={3} onClick={()=>this.gamePlay('scissors')}>
                                    <i className="far fa-hand-scissors"></i>
                                    <p>scissors</p>
                                    </Col>
                                </Row>
                            </Col>
                        </Col>
                    </Modal.Body>
                    <Modal.Footer style={{margin : 'auto'}}>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default Game1;