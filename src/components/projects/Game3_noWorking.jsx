import React, { Component } from 'react';
import { Col, Row, Modal, Button, InputGroup, FormControl } from 'react-bootstrap';
import Words from './gameData';
import './css/Game2.css';

const wordsLibrary = ["pera"];

class Game1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            choosenWord : '',
            userInput : '',
            currentWord : '',
            show: false,
        }
        this.gamePlay = this.gamePlay.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    handleChange(event) {
        this.setState({userInput: event.target.value});
      }


    gamePlay(value) {
        //If there's no word, then choose one
        if(this.state.choosenWord.length === 0){
            let randomNumber = Math.floor(Math.random() *  wordsLibrary.length);
            let randomWord = wordsLibrary[randomNumber];

            this.setState({
                choosenWord : randomWord,
            })
        }

        if(this.state.choosenWord.length > 0){
            //If there's a word, then execute this

            if(this.state.choosenWord.indexOf(value) > 0){
                let wordString = Array.from(this.state.choosenWord); //Convert the word to string
                let foundWord = wordString.indexOf(value); //find the index of the word choosen by the user
                let finalWord = [];
                finalWord.splice(foundWord, 0, value)
                
                return state.concat([finalWord]);
            }
            if(this.state.choosenWord.indexOf(value) < 0){
                console.log("This word is not here, sorry")
            }
            
        }
        else{
            console.log("You still have a word in the state, and this word is "+this.state.choosenWord);
        }
    }
    render() {
        //Game Logic
        console.log(this.state.choosenWord)
        return (
            <>
                <Col>
                    <p style={{ textAlign: 'justify' }}>
                        Hangman juego <code>event.target.value</code>.
                </p>
                </Col>
                <Col md={12} style={{ textAlign: 'center' }}>
                <br/>
                    <Button variant="secondary" onClick={this.handleShow}>
                        Launch Game
                </Button>
                </Col>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header className="game-modal-title">
                        <Modal.Title style={{ margin: 'auto' }}>Random Number</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Col className="game2_display">
                            <Row>
                                <Col>
                                Hombre Colgado
                                </Col>
                                <Col>
                                <Button variant="outline-primary" onClick={()=>this.gamePlay('a')}>A</Button>
                                <Button variant="outline-primary" onClick={()=>this.gamePlay('r')}>R</Button>
                                <Button variant="outline-primary" onClick={()=>this.gamePlay('p')}>P</Button>
                                <Button variant="outline-primary" onClick={()=>this.gamePlay('e')}>E</Button>
                                <Button variant="outline-primary" onClick={()=>this.gamePlay('n')}>N</Button>
                                </Col>
                            </Row>
                            <Col>
                                    <InputGroup className="mb-1">
                                    <FormControl aria-describedby="basic-addon1" type="number" value={this.state.userInput} onChange={this.handleChange} style={{textAlign : 'center'}}/>
                                        <InputGroup.Prepend>
                                            <Button variant="outline-primary" onClick={()=>this.gamePlay()}>Go</Button>
                                        </InputGroup.Prepend>
                                    </InputGroup>
                            </Col>
                        </Col>
                    </Modal.Body>
                    <Modal.Footer style={{ margin: 'auto' }}>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default Game1;