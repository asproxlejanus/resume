import React, { Component } from 'react';
import { Col, Modal, Button, InputGroup, FormControl } from 'react-bootstrap';

import './css/Game2.css';

class Game1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            randomNumber: '',
            userInput : 0,
            show: false,
        }
        this.gamePlay = this.gamePlay.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    handleChange(event) {
        this.setState({userInput: event.target.value});
      }


    gamePlay() {
        let randomNumber = Math.floor(Math.random() * this.state.userInput * 1);
        this.setState({
            randomNumber : randomNumber,
        })
    }
    render() {
        //Game Logic
        return (
            <>
                <Col>
                    <p style={{ textAlign: 'justify' }}>
                        Ejemplo simple de  <code>event.target.value</code>.
                </p>
                </Col>
                <Col md={12} style={{ textAlign: 'center' }}>
                <br/>
                    <Button variant="secondary" onClick={this.handleShow}>
                        Launch Game
                </Button>
                </Col>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header className="game-modal-title">
                        <Modal.Title style={{ margin: 'auto' }}>Random Number</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Col className="game2_display">

                        </Col>
                    </Modal.Body>
                    <Modal.Footer style={{ margin: 'auto' }}>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default Game1;