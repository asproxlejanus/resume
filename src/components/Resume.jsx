import React, { Component } from 'react';
import { Col, Row, ProgressBar } from 'react-bootstrap';
import '../css/Resume.css';

class Resume extends Component {
    state = {}
    render() {
        return (
            <Col className="resume-container" md={12}>
                <Row>
                    <Col className="content-page-title" md={12}>
                        <h4>Curriculum</h4>
                    </Col>
                    <Col className="resume-content-left">
                        <Col className="resume-experience-title">
                            <h5>Experiencia</h5>
                        </Col>
                        <Col className="resume-experience">
                            <p className="resume-date-main">
                                2019 - 2016
                            </p>
                            <hr style={{ marginTop: '-10px' }} />
                            <p className="resume-position">
                                PROPIETÁRIO
                            </p>
                            <p className="resume-company">
                                Café del Llac - Puigcerdà
                            </p>
                            <p className="resume-job-description">
                               Elaboración del plan de empresa, diseño y creación de la marca, puesta en marcha del negócio.
                            </p>
                        </Col>
                        <Col className="resume-experience" style={{ marginTop: '-12px' }}>
                            <p className="resume-date">
                                2016 - 2014
                            </p>
                            <hr style={{ marginTop: '-10px' }} />
                            <p className="resume-position">
                                SUPERVISOR
                            </p>
                            <p className="resume-company">
                                Sellbytel Group - Barcelona
                            </p>
                            <p className="resume-job-description">
                                Supervisión del equipo, gestión de horários, reuniones y seguimiento del personal.
                            </p>
                        </Col>
                        <Col className="resume-experience" style={{ marginTop: '-12px' }}>
                            <p className="resume-date">
                                2014 - 2013
                            </p>
                            <hr style={{ marginTop: '-10px' }} />
                            <p className="resume-position">
                                SOPORTE TÉCNICO AL CLIENTE
                            </p>
                            <p className="resume-company">
                                Sellbytel Group - Barcelona
                            </p>
                            <p className="resume-job-description">
                                Atención al cliente en portugués, inglés y castellano, gestión de herramientas de ticketing (Salesforce).
                            </p>
                        </Col>
                        <Col className="resume-experience" style={{ marginTop: '-12px' }}>
                            <p className="resume-date">
                                2012 - 2014
                            </p>
                            <hr style={{ marginTop: '-10px' }} />
                            <p className="resume-position">
                                SOPORTE TÉCNICO AL CLIENTE
                            </p>
                            <p className="resume-company">
                                Teletech - Barcelona
                            </p>
                            <p className="resume-job-description">
                                Atención al cliente dando soporte a periféricos y tramitación de garantías.
                            </p>
                        </Col>
                    </Col>
                </Row>
                <Row>
                    <Col className="resume-content-left" md={12}>
                        <Col className="resume-experience-title-right">
                            <h5>Estudios</h5>
                        </Col>
                        <Col className="resume-experience" >
                            <p className="resume-date-main">
                                2019
                            </p>
                            <hr style={{ marginTop: '-10px' }} />
                            <p className="resume-position">
                                FULL STACK WEB DEVELOPER (215h)
                            </p>
                            <p className="resume-company">
                                Fundació Esplai - Barcelona
                            </p>
                            <p className="resume-job-description">
                                - Introducción a La Programación Web<br />
                                - Front-End con React JS <br />
                                - Bases de datos. Back-End con NodeJS, Express y MySQL <br />
                                - Competencias transversales: trabajo en equipo, comunicación y liderazgo<br />
                            </p>
                        </Col>
                        <Col className="resume-experience" style={{ marginTop: '-12px' }} >
                            <p className="resume-date">
                                2012 - 2010
                            </p>
                            <hr style={{ marginTop: '-10px' }} />
                            <p className="resume-position">
                                TÉCNICO EN SALUT AMBIENTAL
                            </p>
                            <p className="resume-company">
                                Ciclo Formativo de Grado Superior - Barcelona
                            </p>
                            <p className="resume-job-description">
                                - Aguas de uso y consumo<br />
                                - Gestion de residuos <br />
                                - Contaminación acustica y atmosférica<br />
                                - Control de alimentos<br />
                            </p>
                        </Col>
                        <Col className="resume-experience" style={{ marginTop: '-12px' }} >
                            <p className="resume-date">
                                2006 - 2004
                            </p>
                            <hr style={{ marginTop: '-10px' }} />
                            <p className="resume-position">
                                BACHILLERATO
                            </p>
                            <p className="resume-company">
                                Instituto Jean Piaget - Umuarama - Brasil
                            </p>
                            <p className="resume-job-description">
                            </p>
                        </Col>
                    </Col>
                </Row>
                <Row>
                    <Col className="resume-content-left" md={12} style={{ marginTop: '20px' }}>
                        <Col className="resume-experience-title-right">
                            <h5>Idiomas</h5>
                        </Col>
                        <Row>
                            <Col className="resume-experience" md={3} style={{ left: '15px', textAlign: 'center', marginLeft: '10px' }}>
                                <p className="resume-date-main">
                                    PORTUGUÉS
                            </p>
                                <hr style={{ marginTop: '-10px' }} />
                                <p className="resume-position">
                                    NATIVO
                            </p>
                            </Col>
                            <Col className="resume-experience" md={3} style={{ left: '15px', textAlign: 'center', marginLeft: '5px' }}>
                                <p className="resume-date-main">
                                    CASTELLANO
                            </p>
                                <hr style={{ marginTop: '-10px' }} />
                                <p className="resume-position">
                                    BILINGÜE
                            </p>
                            </Col>
                            <Col className="resume-experience" md={2} style={{ left: '15px', textAlign: 'center', marginLeft: '5px' }}>
                                <p className="resume-date-main">
                                    INGLÉS
                            </p>
                                <hr style={{ marginTop: '-10px' }} />
                                <p className="resume-position">
                                    AVANZADO
                            </p>
                            </Col>
                            <Col className="resume-experience" md={3} style={{ left: '15px', textAlign: 'center', marginLeft: '5px' }}>
                                <p className="resume-date-main">
                                    CATALAN
                            </p>
                                <hr style={{ marginTop: '-10px' }} />
                                <p className="resume-position">
                                    AVANZADO
                            </p>
                            </Col>
                        </Row>

                    </Col>
                </Row>
                <Row>
                    <Col className="resume-content-left" md={12}>
                        <Col className="resume-experience-title-right" style={{ marginTop: '20px' }}>
                            <h5>Habilidades</h5>
                        </Col>
                        <Row>
                            <Col className="resume-experience" md={5} style={{ left: '15px', textAlign: 'center', marginLeft: '26px' }}>
                                <p className="resume-date-main-skills">
                                    DISEÑO
                            </p>
                                <hr style={{ marginTop: '-10px' }} />
                                <p className="resume-skills-text-coding">PHOTOSHOP</p>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={95} className="resume-skills-progress-coding" /></span>
                                <p className="resume-skills-text-coding">INDESIGN</p>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={65} className="resume-skills-progress-coding" /></span>
                                <p className="resume-skills-text-coding">CREATIVIDAD</p>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={100} className="resume-skills-progress-coding" /></span>

                            </Col>
                            <Col className="resume-experience" md={5} style={{ left: '15px', textAlign: 'center', marginLeft: '35px' }}>
                                <p className="resume-date-main-skills">
                                    CODING
                            </p>
                                <hr style={{ marginTop: '-10px' }} />
                                <p className="resume-skills-text-coding">HTML5</p>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={95} className="resume-skills-progress-coding" /></span>
                                <p className="resume-skills-text-coding">CSS3</p>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={90} className="resume-skills-progress-coding" /></span>
                                <p className="resume-skills-text-coding">jQuery/Javascript</p>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={70} className="resume-skills-progress-coding" /></span>
                                <p className="resume-skills-text-coding">ReactJS</p>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={65} className="resume-skills-progress-coding" /></span>
                            </Col>
                        </Row>
                        <Col className="resume-experience" md={11} style={{ left: '15px', textAlign: 'center', marginLeft: '10px' }}>
                            <p className="resume-date-main-skills">
                                TRANSVERSALES
                            </p>
                            <hr style={{ marginTop: '-10px' }} />

                            <Col md={4} className="resume-skills-box-skills">
                                <p className="resume-skills-text">GESTION DE PERSONAL</p>
                                <p className="resume-skills-text">LIDERAZGO</p>
                                <p className="resume-skills-text">TRABAJO EN EQUIPO</p>
                                <p className="resume-skills-text">PROACTIVIDAD</p>
                            </Col>
                            <Col md={4} className="resume-skills-box">
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={90} className="resume-skills-progress" /></span>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={100} className="resume-skills-progress" /></span>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={90} className="resume-skills-progress" /></span>
                                <span className="resume-skills-bar"><ProgressBar variant="warning" now={90} className="resume-skills-progress" /></span>
                            </Col>
                        </Col>
                    </Col>
                </Row>
                <Row>
                    <Col className="resume-content-left" md={12}>
                        <Col className="resume-experience-title-right" style={{marginTop : '20px'}}>
                            <h5>Otros datos de interés</h5>
                        </Col>
                        <Col className="resume-experience" md={11} style={{ left: '15px', textAlign: 'center', marginLeft: '10px' }}>
                            <p className="resume-date-main-skills">
                                Carnet de conducir B
                            </p>
                            <hr style={{ marginTop: '-10px' }} />
                        </Col>
                    </Col>
                </Row>
            </Col>
        );
    }
}

export default Resume;