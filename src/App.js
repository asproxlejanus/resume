import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, withRouter } from "react-router-dom";
import { Container, Col } from 'react-bootstrap';
import { connect } from 'react-redux';

//Pages imports

import AppHeader from './components/AppHeader'
import About from './components/About';
import Resume from './components/Resume';
import Projects from './components/Projects';
import Contact from './components/Contact';
import NotFound from './components/NotFound';

// Import styles

import './css/Main.css';
import './css/screen1024.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: '',
      closed: '',
      redirect : false,
    };

    this.close = this.close.bind(this);
    this.slideUp = this.slideUp.bind(this);
  }

  close() {
    this.setState({
      open: false,
      closed : true,
      redirect : true,
    })
   // this.history.push('/')
  }


  slideUp() {
  this.setState({
    open : true,
    closed : false,
  })
  }


  render() {

    let slideDownAnimation = '';
    let closeButton = '';

    if(this.state.open === true){
      slideDownAnimation = 'slide-animation-down'
      closeButton = 'app-close-button';
    }
    if(this.state.open === false && this.state.closed !== false){
      slideDownAnimation = 'slide-animation-close'
      closeButton = '';
    }

    //<Redirect to='/' />
    
    return (
      <BrowserRouter>
        <Container>
          <Col className="app-container" style={{ margin: "auto" }} md={12}>
            <Col md={10} className="app-header" style={{ margin: "auto" }}>
              <AppHeader state={this.state} slideUp={this.slideUp}/>
            </Col>
            <Col md={9} className={slideDownAnimation + " app-page "} style={{ margin: "auto" }}>
              <Switch>
                <Route path="/about" component={About} />
                <Route path="/resume" component={Resume} />
                <Route path="/projects" component={Projects} />
                <Route path="/contact" component={Contact} />
                <Route component={NotFound} />
              </Switch>
            </Col>
            <Col className={closeButton + " app-page-close-button "} onClick={() => this.close()}>Close</Col>
          </Col>
        </Container>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    SlideUp: state
  }
}

export default connect(mapStateToProps)(App);
