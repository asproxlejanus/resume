const NavReducer = (state = {}, action) => {
    switch(action.type) {
      case 'SLIDER_UPDATE':
        state = action.data;
        return state;
      default:
        return state;
    }
  }
  export default NavReducer;